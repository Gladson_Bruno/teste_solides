-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 20-Nov-2017 às 01:12
-- Versão do servidor: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testesolides`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `marcacoesusuarios`
--

DROP TABLE IF EXISTS `marcacoesusuarios`;
CREATE TABLE IF NOT EXISTS `marcacoesusuarios` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CpfUsuario` double NOT NULL,
  `DataMarcacao` varchar(50) DEFAULT NULL,
  `MarcacaoEntrada` varchar(50) DEFAULT NULL,
  `MarcacaoAlmoco` varchar(50) DEFAULT NULL,
  `MarcacaoVoltaAlmoco` varchar(50) DEFAULT NULL,
  `MarcacaoSaida` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `marcacoesusuarios`
--

INSERT INTO `marcacoesusuarios` (`Id`, `CpfUsuario`, `DataMarcacao`, `MarcacaoEntrada`, `MarcacaoAlmoco`, `MarcacaoVoltaAlmoco`, `MarcacaoSaida`) VALUES
(2, 10251900681, '19/11/2017', '08:00:00', '12:00:00', '13:00:58', '18:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) DEFAULT NULL,
  `Funcao` varchar(50) DEFAULT NULL,
  `Cpf` double DEFAULT NULL,
  `Senha` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`Id`, `Nome`, `Funcao`, `Cpf`, `Senha`) VALUES
(4, 'Gladson', 'Estagiario', 10251900681, '202cb962ac59075b964b07152d234b70');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
