<?php

Class Cadastrar extends CI_Controller{
  public function __construct(){
    parent::__construct();
    //Carregando a model
    $this->load->model('Cadastrar_model');
  }




  public function index(){
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->library('form_validation');//Carregando a biblioteca form validation
    $data['title'] = 'Cadastrar Usuário';
    $data['erro'] = '';

    $this->form_validation->set_rules('nomeUsuario', 'NOME', 'required');
    $this->form_validation->set_rules('funcaoUsuario', 'FUNÇÃO', 'required');
    $this->form_validation->set_rules('cpfUsuario', 'CPF', 'required');
    $this->form_validation->set_rules('senhaUsuario', 'SENHA', 'required');

    if($this->form_validation->run() === false){
      $this->load->view('templates/header', $data);
      $this->load->view('pages/cadastrar', $data);
      $this->load->view('templates/footer');
    }else{
      if($this->Cadastrar_model->cadastrar() == true){
        
        $data['info'] = 'Usuário '.$this->input->post('nomeUsuario').' cadastrado com sucesso';
        $this->load->view('templates/header', $data);
        $this->load->view('pages/cadastro-sucesso', $data);
        $this->load->view('templates/footer');

      }else{

        $data['title'] = "Erro ao realizar o cadastro";
        $data['erro'] = "Cpf inválido, tente novamente";
  
        $this->load->view('templates/header', $data);
        $this->load->view('pages/erro-cadastro', $data);
        $this->load->view('templates/footer');

      }
    }
  }
}

 ?>
