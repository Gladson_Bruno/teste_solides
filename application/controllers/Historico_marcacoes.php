<?php
class Historico_marcacoes extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Historico_marcacoes_model');
        $this->load->library('session');
    }

    public function index(){
        $this->load->helper('url');

        $data['title'] = "Histórico Marcações";
        $data['marcacoes'] = $this->Historico_marcacoes_model->get_Marcacoes();
        
        if(empty($this->session->userdata('nomeUsuario'))){
            header("Location:".site_url('login')."");
        }else{
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/historico-marcacao');
            $this->load->view('templates/footer');
        }
    }

    
}





?>