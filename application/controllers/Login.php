<?php
class Login extends CI_Controller{
  public function __construct(){
    parent::__construct();
    //Carregando a model
    $this->load->model('Login_model');
    $this->load->library('session');
  }

  public function index(){

    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->library('form_validation');//Carregando a biblioteca form validation
    $data['title'] = 'Login';
    $data['erro'] = '';

    $this->form_validation->set_rules('cpfUsuario', 'CPF', 'required');
    $this->form_validation->set_rules('senhaUsuario', 'SENHA', 'required');

    if($this->form_validation->run() === false){
      $this->load->view('templates/header', $data);
      $this->load->view('pages/login');
      $this->load->view('templates/footer');
    }
    else{
      if($this->Login_model->validarLogin()['Id'] != null){
        //Pegando informações do usuario
        $cpfUsuario = $this->Login_model->validarLogin()['Cpf'];
        $nomeUsuario = $this->Login_model->validarLogin()['Nome']; 

          //definindo dados da sessão
          $dadosSessao = array('Cpf'=> $cpfUsuario, 'nomeUsuario' => $nomeUsuario);

          //Dados da sessão expiram em 5 minutos
          $this->session->set_tempdata($dadosSessao, 600);

          $data['title'] = 'Index';

          $this->load->view('templates/header-index', $data);
          $this->load->view('pages/home');
          $this->load->view('templates/footer');
      }else{
        $data['erro'] = 'Usuário ou senha incorretos, tente novamente.';
        $this->load->view('templates/header', $data);
        $this->load->view('pages/erro-login', $data);
        $this->load->view('templates/footer');

      }


      //$this->load->view('pages/index');
    }

  }


}

 ?>
