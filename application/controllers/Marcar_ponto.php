<?php
class Marcar_ponto extends CI_Controller{
    public function __construct(){
        parent::__construct();
        //Carregando a model
        $this->load->model('Marcar_ponto_model');
        $this->load->library('session');
    }

    public function index(){
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');//Carregando a biblioteca form validation
        $data['title'] = 'Marcação de Ponto';
        $data['erro'] = '';

        //Definindo que o relogio sera de acordo com o usuario e não com o servidor
        date_default_timezone_set('America/Sao_Paulo');

        
        $this->form_validation->set_rules('tipoMarcacao', 'Tipo Marcação', 'required');

        if($this->form_validation->run() === false){
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/marcar-ponto');
            $this->load->view('templates/footer');
        }else{
            $this->Marcar_ponto_model->marcar_ponto();
        }
    }
}


?>