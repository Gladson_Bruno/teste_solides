<?php

class Home extends CI_Controller{
    public function __construct(){
        parent::__construct();
        //carregando a model
        $this->load->model('Home_model');
        $this->load->library('session');
    }

    public function index(){
        $this->load->helper('url');
        $data['title'] = 'Home';
        
        
        $this->load->view('templates/header-index',$data);
        $this->load->view('pages/home');
        $this->load->view('templates/footer');

    }

    
    public function view($page = 'home'){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');//Carregando a biblioteca form validation
        
        $data['title'] = ucfirst($page);

        $this->load->view('templates/header-index',$data);
        $this->load->view('pages/'.$page);
        $this->load->view('templates/footer');
    }
    

}


?>