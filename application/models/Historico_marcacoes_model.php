<?php
class Historico_marcacoes_model extends CI_Model{
    
    
    public function __construct(){
        $this->load->database();
        $this->load->library('session');
    }

    function get_Marcacoes(){
        $cpfUsuario = $this->session->userdata('Cpf');

        $query = $this->db->query('select * from marcacoesusuarios where cpfUsuario = '.$cpfUsuario.'');

        return $query->result_array();

    }

    function calcularTempoTrabalho($hora_inicial, $hora_final, $inicio_almoco, $fim_almoco) {
        
        $i = 1;
        $tempo_total;
        
        $tempos = array($hora_final, $hora_inicial, $inicio_almoco, $fim_almoco);
        
        foreach($tempos as $tempo) {
        $segundos = 0;
        
        list($h, $m, $s) = explode(':', $tempo);
        
        $segundos += $h * 3600;
        $segundos += $m * 60;
        $segundos += $s;
        
        $tempo_total[$i] = $segundos;
        
        $i++;
        }
        $segundos = ($tempo_total[1] - $tempo_total[2]) + ($tempo_total[3] - $tempo_total[4]); 
        
        $horas = floor($segundos / 3600);
        $horas = (strlen($horas) == 1) ? "0".$horas : $horas;
        $segundos -= $horas * 3600;
        $minutos = str_pad((floor($segundos / 60)), 2, '0', STR_PAD_LEFT);
        $segundos -= $minutos * 60;
        $segundos = str_pad($segundos, 2, '0', STR_PAD_LEFT);
        
        return $horas.':'.$minutos.':'.$segundos;
        }

}



?>