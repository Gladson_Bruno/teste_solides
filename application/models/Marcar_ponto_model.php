<?php
class Marcar_ponto_Model extends CI_Model{
    
    public $nomeUsuario;
    public $cpfUsuario;
    public $dataMarcacao;
    public $horaMarcacao;
    public $tipoMarcacao;
    public $query;


    public function __construct(){
        //Carregando base de dados
        $this->load->database();
        $this->load->library('session');
    }
    

    //implementar
    public function marcar_ponto(){
        addslashes($nomeUsuario = $this->session->userdata('nomeUsuario'));
        addslashes($cpfUsuario = $this->session->userdata('Cpf'));
                   $dataMarcacao = date('d/m/Y');
                   $horaMarcacao = date('H:i:s');
        addslashes($tipoMarcacao = $this->input->post('tipoMarcacao'));

        $informacoes = array(
            "nome" => $nomeUsuario,
            "cpf" => $cpfUsuario,
            "data" => $dataMarcacao,
            "hora" => $horaMarcacao,
            "tipo" => $tipoMarcacao
        );

        $query = $this->db->query("select * from marcacoesusuarios where CpfUsuario = ".$cpfUsuario." and DataMarcacao = '".$dataMarcacao."'");

        if($tipoMarcacao == 'Entrada'){

            return $this->verificar_Entrada($query, $informacoes);

        }else if($tipoMarcacao == 'InicioAlmoco'){

            return $this->verificar_Inicio_Almoco($query, $informacoes);

        }else if($tipoMarcacao == 'FimAlmoco'){
            
            return $this->verificar_Final_Almoco($query, $informacoes);

        }else if($tipoMarcacao == 'Saida'){
            //Implementar
            return $this->verificar_Saida($query, $informacoes);
        }

    }

    public function verificar_Entrada($q, $info){

        if($q->row_array()['MarcacaoEntrada'] == null ){
            $data = array(
                'CpfUsuario' => $info['cpf'],
                'DataMarcacao' => $info['data'],
                'MarcacaoEntrada' => $info['hora']
            );
            $this->db->insert('marcacoesusuarios', $data);

            $data['title'] = "Marcação realizada com sucesso";
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/marcacao-sucesso', $data);
            $this->load->view('templates/footer');           
        }else{
            $data['erro'] = 'Já existe uma marcação de entrada nesta data, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');           
        }
    }

    function verificar_Inicio_Almoco($q, $info){

        //$query = $this->db->query("update marcacoesusuarios set MarcacaoAlmoco = '".$horaMarcacao."' where CpfUsuario = ".$cpfUsuario."");
        if(empty($q->row_array())){
            $data['erro'] = 'Não existe marcação de entrada, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');           
        }else if($q->row_array()['MarcacaoAlmoco'] == null){
            $dados = array(
                'MarcacaoAlmoco' => $info['hora']
            );

            $condicoes = array('CpfUsuario' => $info['cpf'], 'DataMarcacao' => $info['data']);
            $this->db->where($condicoes);
            $this->db->update('marcacoesusuarios', $dados);

            $data['title'] = "Marcação realizada com sucesso";
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/marcacao-sucesso', $data);
            $this->load->view('templates/footer');
        }else{
            $data['erro'] = 'Já existe uma marcação de inicio de almoço nesta data, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');           
        }

    }

    function verificar_Final_Almoco($q, $info){
        if(empty($q->row_array())){

            $data['erro'] = 'Não existe marcação de entrada, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');           

        }else if($q->row_array()['MarcacaoAlmoco'] == null){

            $data['erro'] = 'Não existe marcação de inicio de almoço, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');           

        }else if($q->row_array()['MarcacaoVoltaAlmoco'] == null){
     
            $dados = array(
                'MarcacaoVoltaAlmoco' => $info['hora']
            );

            $condicoes = array('CpfUsuario' => $info['cpf'], 'DataMarcacao' => $info['data']);
            $this->db->where($condicoes);
            $this->db->update('marcacoesusuarios', $dados);

            $data['title'] = "Marcação realizada com sucesso";
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/marcacao-sucesso', $data);
            $this->load->view('templates/footer');

        }else{
            $data['erro'] = 'Já existe uma marcação de termino de almoço nesta data, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');    
        }
    }

    function verificar_Saida($q, $info){
        if(empty($q->row_array())){

            $data['erro'] = 'Não existe marcação de entrada, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');           

        }else if($q->row_array()['MarcacaoAlmoco'] == null){

            $data['erro'] = 'Não existe marcação de inicio de almoço, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer'); 

        }else if($q->row_array()['MarcacaoVoltaAlmoco'] == null){
            
            $data['erro'] = 'Não existe marcação de termino de almoço, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');

        }else if($q->row_array()['MarcacaoSaida'] == null){
            
            $dados = array(
                'MarcacaoSaida' => $info['hora']
            );

            $condicoes = array('CpfUsuario' => $info['cpf'], 'DataMarcacao' => $info['data']);
            $this->db->where($condicoes);
            $this->db->update('marcacoesusuarios', $dados);

            $data['title'] = "Marcação realizada com sucesso";
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/marcacao-sucesso', $data);
            $this->load->view('templates/footer');
        }else{
            $data['erro'] = 'Já existe uma marcação de saida nesta data, tente novamente';
            $data['title'] = 'Erro ao realizar marcação';
            $this->load->view('templates/header-index', $data);
            $this->load->view('pages/erro-marcacao', $data);
            $this->load->view('templates/footer');    
        }
    }

}



?>