<div class="container">
  <a href="<?php echo site_url('login')?>"><button class="btn btn-danger" style="float: right">Voltar</button></a>
  <div class="jumbotron">

    <h1><?php echo $title ?></h1>

    <!-- Form validator para validar formulario -->
    <?php echo validation_errors(); ?>
    <!--Abrindo formulario-->
    <?php echo form_open('Cadastrar/index') ?>
    <div class="form-group">
      <label for="nomeUsuario">Nome:</label>
      <input type="text" class="form-control" name="nomeUsuario" placeholder="Digite seu nome">
      <br/>
    </div>

    <div class="form-group">
      <label for="funcaoUsuario">Função:</label>
      <input type="text" class="form-control" name="funcaoUsuario" placeholder="digite sua função">
      <br/>
    </div>

    <div class="form-group">
      <label for="cpfUsuario">CPF:</label>
      <input type="text" class="form-control" name="cpfUsuario" placeholder="digite seu CPF">
      <br/>
    </div>

    <div class="form-group">
      <label for="senhaUsuario">Senha:</label>
      <input type="password" class="form-control" name="senhaUsuario" placeholder="digite sua função">
      <br/>
    </div>

    <input type="submit" class="btn btn-info" value="Cadastrar"/>

  </form>

  </div>
</div>
