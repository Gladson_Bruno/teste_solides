<div class="container">
<a href="<?php echo site_url('login');?>"><button class="btn btn-danger" style="float: right">Logoff</button></a>    
    <div class="jumbotron">
        <h1><?php echo $title?></h1>
    </div>

    <table class="table table-inverse">
        <thead>
            <tr>
                <th>Data Marcação</th>
                <th>Entrada Empresa</th>
                <th>Inicio Almoço</th>
                <th>Volta Almoço</th>
                <th>Saída Empresa</th>
                <th>Horas Trabalhadas</th>
            </tr>
        </thead>
        <tbody>

            <?php foreach($marcacoes as $item): ?>

                <tr>
                    <td><?php  if(empty($item['DataMarcacao'])) echo " "; else echo $item['DataMarcacao'] ?></td>
                    <td><?php if(empty($item['MarcacaoEntrada'])) echo " "; else echo $item['MarcacaoEntrada']?></td>
                    <td><?php if(empty($item['MarcacaoAlmoco'])) echo " "; else echo $item['MarcacaoAlmoco']?></td>
                    <td><?php if(empty($item['MarcacaoVoltaAlmoco'])) echo " "; else echo $item['MarcacaoVoltaAlmoco']?></td>
                    <td><?php if(empty($item['MarcacaoSaida'])) echo " "; else echo $item['MarcacaoSaida']?></td>
                    <td>
                        <?php 
                        if(!empty($item['MarcacaoSaida'])){
                            echo $this->Historico_marcacoes_model->calcularTempoTrabalho($item['MarcacaoEntrada'],$item['MarcacaoSaida'], $item['MarcacaoAlmoco'], $item['MarcacaoVoltaAlmoco']);
                        }
                        ?>
                    </td>
                </tr>

            <?php endforeach; ?>

        </tbody>
    </table>

</div>