<div class="container">
  <div class="jumbotron">

    <h1><?php echo $title ?></h1>

    <!-- Form validator para validar formulario -->
    <?php echo validation_errors(); ?>
    <!--Abrindo formulario-->
    <?php echo form_open('Login/index') ?>
    <div class="form-group">
      <label for="loginUsuario">CPF:</label>
      <input type="text" class="form-control" name="cpfUsuario" placeholder="Digite seu cpf">
      <br/>
    </div>

    <div class="form-group">
      <label for="senhaUsuario">Senha:</label>
      <input type="password" class="form-control" name="senhaUsuario" placeholder="digite sua senha">
      <br/><br/>
    </div>
    <input type="submit" class="btn btn-info" value="Logar"/>

  </form>

      <a href="<?php echo site_url('cadastrar')?>"><button class="btn btn-success" id="cadastrar">Cadastrar</button></a>
  </div>

  

</div>
