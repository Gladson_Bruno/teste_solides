<div class="container">
    <a href="<?php echo site_url('login');?>"><button class="btn btn-danger" style="float: right">Logoff</button></a>
    <div class="jumbotron">
    
        <h1><?php echo $title ?></h1>

        <!-- Erros Form Validator -->
        <?php echo validation_errors(); ?>
        <?php echo form_open('marcar_ponto/index')?>

        <div class="form-group">
            <label for="nomeUsuario">Nome Usuario:</label>
            <input type="text" class="form-control" name="nomeUsuario" value="<?php echo $this->session->userdata('nomeUsuario');?>" disabled>
        </div>

        <div class="form-group">
            <label for="dataMarcacao">Data:</label>
            <input type="text" class="form-control" name="dataMarcacao" value="<?php echo date('d/m/Y');?>" disabled>
        </div>
        

        
        <div class="form-group">
            <label for="horaMarcacao">Hora:</label>
            <input type="text" class="form-control" name="horaMarcacao" value="<?php echo date('H:i:s');?>" disabled>
        </div>


        <div class="form-group">
            <label for="tipoMarcacao">Tipo Marcação:</label>
            <select class="form-control" name="tipoMarcacao" style="height: 34">
                <option value=""></option>
                <option value="Entrada">Entrada</option>
                <option value="InicioAlmoco">Inicio Almoço</option>
                <option value="FimAlmoco">Fim Almoço</option>
                <option value="Saida">Saida</option>
            </select>
        </div>

        <input type="submit" class="btn btn-success" value="Registrar"/>
        </form>
    
    </div>
</div>