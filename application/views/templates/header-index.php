<html>
  <head>
    <title> <?php echo $title ?> </title>
    <!--Importando bootstrap 4-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <!--Importando bootstrap 3-->
    <!-- Última versão CSS compilada e minificada -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Tema opcional -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- Última versão JavaScript compilada e minificada -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

  <style>
  body {
    background: url("https://acesso.oab.org.br/content/img/bg-login-clubedeservicos.jpg") no-repeat center center fixed;
    background-size: cover;
    background-attachment: fixed;
    padding-top: 50px;
    position: relative;
 }  
  pre {
      tab-size: 8;
  }
  
  @media screen and (max-width: 768px) {
      .side-collapse-container{
          width:100%;
          position:relative;
          left:0;
          transition:left .4s;
      }
      .side-collapse-container.out{
          left:200px;
      }
      .side-collapse {
          top:50px;
          bottom:0;
          left:0;
          width:200px;
          position:fixed;
          overflow:hidden;
          transition:width .4s;
      }
      .side-collapse.in {
          width:0;
      }

      #menu{
        padding: 10px;
      }
  }
  </style>

  <script>
    $(document).ready(function() {   
        var sideslider = $('[data-toggle=collapse-side]');
        var sel = sideslider.attr('data-target');
        var sel2 = sideslider.attr('data-target-2');
        sideslider.click(function(event){
        $(sel).toggleClass('in');
        $(sel2).toggleClass('out');
      });
    });
  </script>

  </head>

  <body>

    <header role="banner" class="navbar navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-left"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="navbar-inverse side-collapse in">
          <nav role="navigation" class="navbar-collapse">

              <a href="<?php echo site_url('home')?>"><h4 class="navbar-text" id="menu" style="color:white; float: left"> Index </h4></a>
              <a href="<?php echo site_url('marcar-ponto')?>"><h4 class="navbar-text" id="menu" style="color:white; float: left"> Marcar Ponto </h4></a>
              <a href="<?php echo site_url('historico-ponto')?>"><h4 class="navbar-text" id="menu" style="color:white; float: left"> Histórico de Marcações </h4></a>

          </nav>
        </div>
      </div>
    </header>

  <br /><br /><br /><br /><!--Espaçamento entre borda e divs-->

  <!-- Verifica se tem algum usuario na sessão -->
  <?php if($this->session->userdata('nomeUsuario') == null ) header("Location:".site_url('login')."")?>