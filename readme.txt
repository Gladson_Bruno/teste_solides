=================================================================================================================
=================================================================================================================
                        Sistema de registro de horas de trabalho de estagiários.
                        
                        Autor: Gladson Bruno Soares
=================================================================================================================



Primeiros Passos
=================================================================================================================

*Para fazer o sistema funcionar primeiramente deve-se importar o banco de dados do arquivo Dump_testesolides.sql
ou então criar manualmente as tabelas com base no arquivo estrutura-do-banco.txt

*Os arquivos do projeto devem ser descompactados na pasta do servidor.


*Caso esteja usando algum servidor como o wampp ou xampp:

*No wampp server o projeto deve ser descompactado na pasta www e aberto no browser como localhost/pasta_projeto
*caso seu localhost seja diferente passar localhost_usado/pasta_projeto.

*No xampp server o projeto deve ser descompactado na pasta htdocs e aberto no browser como localhost/pasta_projeto
*caso seu localhost seja diferente passar localhost_usado/pasta_projeto.


=================================================================================================================

Configurando banco de dados do projeto.
=================================================================================================================

Vá até o seguinte arquivo: pasta_projeto/application/config/database.php
Caso alguma informação seja diferente das usadas pelo seu computador

*Na linha número 78(hostname) deve ser configurado seu hostname. Exemplo: "localhost".
*Na linha número 79(username) deve ser configurado o username do servidor de banco de dados que será usado.
*Na linha número 80(password) deve ser configurado a senha de acesso ao servidor.
*Na linha número 81(database) caso você tenha alterado o nome do banco de dados deve-se colocar o nome do banco
de dados que contém as tabelas.
=================================================================================================================



Executando o projeto
=================================================================================================================

*Abrir no browser o seguinte endereço: 'caminho_servidor/pasta_projeto'.
*Na primeira ver que executado, deve-se criar um usuário para acessar a aplicação.